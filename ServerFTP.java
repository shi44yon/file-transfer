import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.Buffer;

public class Server extends Thread {
	
	private ServerSocket socket;
	
	public Server(int port) {
		try {
			socket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
  
	private void saveFile(Socket ServerSock) throws IOException {
        DataInputStream dis = new DataInputStream(ServerSock.getInputStream());
      
        BufferedReader fromServer = new BufferedReader(new InputStreamReader(ServerSock.getInputStream())); 
        
		FileOutputStream fos = new FileOutputStream("file.zip");
		byte[] buffer = new byte[4096];
		
		int fileSize = 2048 * 1000000000;
		int read = 0;
		int fileRead = 0;
		int fileRemain = fileSize;
      
		while((read = dis.read(buffer, 0, Math.min(buffer.length, fileRemain))) > 0) {
			fileRead += read;
			fileRemain -= read;
			System.out.println(fileRead + " byte(s)");
			fos.write(buffer, 0, read);
		}
		
		fos.close();
		dis.close();
	}
  
	public void startTransfer() {
		while (true) {
			try {
				Socket ServerSock = socket.accept();
				saveFile(ServerSock);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
  
	public static void main(String[] args) {
		Server server = new Server(5656);
		server.startTransfer();
	}
}